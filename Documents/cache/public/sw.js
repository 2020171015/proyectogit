/* eslint-disable no-restricted-globals */
// Instalacion del service worker
/*self.addEventListener('install', function (event) {
    console.log('SW instaldo');

    const instalacion = new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('Termino')
            self.skipWaiting();
            resolve()
        }, 1000);
    });

    event.waitUntil(instalacion);

});

self.addEventListener('activate', function (event) {
    console.log('SW activado 1');
})

self.addEventListener('fetch', function(event) {
    if (event.request.url.includes('https://fakestoreapi.com/products/1')) {
        const resp = new Response(`{"ok":"false", "mensaje":"Este recurso no está disponible"}`);
        event.respondWith(resp);
    }
    // console.log('SW fetch', event.request.url);
});

//Sync recuperar la conexion a internet
self.addEventListener('sync',function(event){
    console.log('tenemos conexion');
    console.log(event);
    console.log(event.tag);
})
*/

/* self.addEventListener('fetch',event => {
    const oflineResponse = new Response(`
        Bienvenido a mi pagina web 
        Disculpa, pero para usarla necesitas internet 2
    `)
    const response = fetch(event.request)
    .catch(() => oflineResponse)

    event.respondWith(response)
}) */

//const CACHE_NAME = 'cache.3'

const CACHE_DYNAMIC = 'dynamic-v1'
const CACHE_STATIC = 'static-v1'
const CACHE_INMUTABLE = 'inmutable.v1'

const limpiarCache = (cacheName, numberItem) => {
    caches.open(cacheName)
      .then(cache => {
        cache.keys()
          .then(keys => {
            if (keys.length > numberItem) {
  
              cache.delete(keys[0])
                .then(limpiarCache(cacheName, numberItem))
  
            }
          })
      })
  }

self.addEventListener('install', function (event) {

  const cahePromise = caches.open(CACHE_STATIC).then(function (cache) {

    return cache.addAll([
      '/',
      '/index.html',
      '/js/app.js',
      '/favicon.ico',
      '/sw.js',
      'static/js/bundle.js'
    ])
  })

  const cacheInmutable = caches.open(CACHE_INMUTABLE).then(function (cache) {
    return cache.addAll([

      'https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css'

    ])
  })
  // event.waitUntil(cahePromise)
  event.waitUntil(Promise.all([cahePromise, cacheInmutable]))
})

/*self.addEventListener('fetch', function(event){
    //Todo es servido desde el cache
    event.respondWith(caches.match(event.request))
})*/

/*
self.addEventListener('fetch', function (event) {
  //Cache with network fallback
  const respuesta = caches.match(event.request)
    .then(response => {
      if (response) return response
      //Si no existe el archivo lo descarga
      return fetch(event.request)
        .then(newResponse => {
          caches.open(CACHE_DYNAMIC)
            .then(cache => {
              cache.put(event.request, newResponse)
              limpiarCache(CACHE_DYNAMIC,2)
            })
          return newResponse.clone()
        })
    })
  event.respondWith(respuesta)
})
*/
self.addEventListener('fetch', function (event) {
    //Cache with network fallback
    const respuesta = fetch(event.request)
        .then(res => {
            caches.open(CACHE_DYNAMIC)
            .then(cache => {
                cache.put(event.request, res)
                limpiarCache(CACHE_DYNAMIC, 5)
            })
            return res.clone()
        }).catch(err => {
            return caches.match(event.request)
        })
    event.respondWith(respuesta)
})