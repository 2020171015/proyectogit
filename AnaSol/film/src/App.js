import React from 'react';
import './App.css';

function App() {
  return (
    <div>
      <header>
        <nav>
          <a href="#">Iniciar Sesión</a>
          <a href="#">Registrarme</a>
          <a href="#">Favoritos</a>
        </nav>
      </header>

      <section>
        <h2>Listado de Películas</h2>
        <div className="movie-list">
          <MovieCard title="Título de la Película 1" description="Descripción de la Película 1." />
          <MovieCard title="Título de la Película 2" description="Descripción de la Película 2." />
        </div>
      </section>
    </div>
  );
}

function MovieCard({ title, description }) {
  return (
    <div className="movie-card">
      <div className="movie-title">{title}</div>
      <div className="movie-description">{description}</div>
    </div>
  );
}

export default App;